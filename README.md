# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/R6R8QT94J)  CorsoBackup Helm Chart

## Overview

This Helm chart installs and configures CorsoBackup, a simple and efficient solution for backing up Microsoft 365 on Kubernetes clusters. CorsoBackup enables you to securely backup and restore your Microsoft 365 data, including emails, calendars, and contacts.

## Prerequisites

- Kubernetes 1.19+
- Helm 3.0+
- PV provisioner support in the underlying infrastructure

## Installation

To install the CorsoBackup Helm chart, run the following command:

```bash
helm install corsobackup ./corsobackup --namespace <your-namespace>
```

Replace `<your-namespace>` with the name of the Kubernetes namespace where you want to install CorsoBackup.

## Configuration

Configuration of CorsoBackup is done via the `values.yaml` file. Here, you can set parameters such as the repository bucket and optional endpoints.

Example of a minimal configuration:

```yaml
repository:
  bucket: my-backup-bucket
  # Optional: Specify an alternative S3 provider like iDrive e2 or Backblaze
  endpoint: https://s3.us-west-002.backblazeb2.com
```

### Important Notes

- The S3 bucket must exist prior to the installation and is NOT automatically created by this chart.

## Backup Jobs

After installation, you can configure backup jobs to backup your Microsoft 365 data. These jobs can be set up via CronJob resources in Kubernetes, defined in the `templates/` directory of the chart.

### Templates Detail

#### Init Job

The `init-job` template is responsible for initializing the backup environment. This may include tasks like setting up the repository in the S3 bucket. The job is executed as a Helm hook at the post-install phase, ensuring that it runs immediately after the chart is installed.

##### Values and Their Usage:

- `image.repository`: The container image repository for the backup job.
- `image.tag`: The specific tag of the container image to use.
- `image.pullPolicy`: The image pull policy for the container image.
- `repository.bucket`: The name of the S3 bucket where backups will be stored.
- `repository.endpoint`: (Optional) The endpoint URL for an alternative S3-compatible service.

The rest of the variables explained by corsobackup itself, please see https://corsobackup.io/docs/intro/

- `AWS_SECRET_ACCESS_KEY`: 
- `AWS_ACCESS_KEY_ID`: 
- `AZURE_TENANT_ID`:
- `AZURE_CLIENT_SECRET`: 
- `AZURE_CLIENT_ID`:
- `CORSO_PASSPHRASE`: 

## License

CorsoBackup and this Helm chart are available under the MIT License.
